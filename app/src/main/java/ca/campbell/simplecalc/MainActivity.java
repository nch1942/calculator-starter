package ca.campbell.simplecalc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

//  TODO: add a field to input a 2nd number, get the input and use it in calculations
//  TODO: the inputType attribute forces a number keyboard, don't use it on the second field so you can see the difference

//  TODO: add buttons & methods for subtract, multiply, divide

//  TODO: add input validation: no divide by zero
//  TODO: input validation: set text to show error when it occurs

//  TODO: add a clear button that will clear the result & input fields

//  TODO: the hint for the result widget is hard coded, put it in the strings file

public class MainActivity extends Activity {
    EditText userNumb1;
    EditText userNumb2;
    TextView result;
    double num1;
    double num2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // get a handle on the text fields
        userNumb1 = (EditText) findViewById(R.id.num1);
        userNumb2 = (EditText) findViewById(R.id.num2);
        result = (TextView) findViewById(R.id.result);
    }  //onCreate()

    // TODO: replace with code that adds the two input numbers
    public void addNums(View v) {
        num1 = Double.parseDouble(userNumb1.getText().toString());
        num2 = Double.parseDouble(userNumb2.getText().toString());

        result.setText(Double.toString(num1 + num2));
    }

    public void subNums(View v) {
        num1 = Double.parseDouble(userNumb1.getText().toString());
        num2 = Double.parseDouble(userNumb2.getText().toString());

        result.setText(Double.toString(num1 - num2));
    }

    public void mulNums(View v) {
        num1 = Double.parseDouble(userNumb1.getText().toString());
        num2 = Double.parseDouble(userNumb2.getText().toString());

        result.setText(Double.toString(num1 * num2));
    }

    public void divNums(View v) {
        num1 = Double.parseDouble(userNumb1.getText().toString());
        num2 = Double.parseDouble(userNumb2.getText().toString());

        if (num2 == 0) {
            result.setText("CANNOT DIVIDE BY ZERO");
            result.setTextColor(getResources().getColor(R.color.warning));
    }
        else {
            result.setText(Double.toString(num1 / num2));
        }
    }

    public void clearNums(View v) {
        userNumb1.setText("");
        userNumb2.setText("");
    }
}